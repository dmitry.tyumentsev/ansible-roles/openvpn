OpenVPN role
=========

Installs and configures [OpenVPN](https://openvpn.net/) server Debian/Ubuntu servers.

Requirements
----------------
[netaddr](https://pypi.org/project/netaddr/)
`pip install netaddr`

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - role: tyumentsev4.openvpn
      vars:
        openvpn_clients:
          - pc
```
